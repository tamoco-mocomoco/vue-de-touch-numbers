import { createStore } from 'vuex'

const BEST_TIME_KEY = 'best_time';

export default createStore({
  state: {
    gameMode: 0,
    timeCount: 0,
    bestTime: localStorage.getItem(BEST_TIME_KEY) ? Number(localStorage.getItem(BEST_TIME_KEY)) : 0
  },
  mutations: {
    changeGameMode(state, modeNum: number) {
      // トランジションのために一時的に表示しないモードを格納
      state.gameMode = -1;
      setTimeout(() => {
        state.gameMode = modeNum;
      }, 1000);
    },
    incrementTimeCount(state) {
      state.timeCount++;
    },
    resetTimeCount(state) {
      state.timeCount = 0;
    },
    updateBestTime(state) {
      if (state.bestTime && state.timeCount < state.bestTime)
        state.bestTime = state.timeCount;
        localStorage.setItem(BEST_TIME_KEY, String(state.timeCount));
    }
  },
  actions: {
    changeGameMode(context, modeNum: number) {
      context.commit('changeGameMode', modeNum);
    },
    incrementTimeCount(context) {
      context.commit('incrementTimeCount');
    },
    resetTimeCount(context) {
      context.commit('resetTimeCount');
    },
    updateBestTime(context) {
      context.commit('updateBestTime');
    }
  },
  modules: {
  }
})
